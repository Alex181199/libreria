$(window).on('load',function () {


    $("#submit-register").click(function  (e) {

        e.preventDefault();
        let nome=$("#nome").val();
        let cognome=$("#cognome").val();
        let Indirizzo=$("#indirizzo").val();
        let cap=$("#CAP").val();
        let citta=$("#citta").val();
        let paese=$("#paese").val();
        let tel=$("#tel").val();
        let mail=$("#email").val();
        let passw=$("#pwd").val();
        let ripassw=$("#pwd-repeat").val();
        let cc=$("#cc").val();
        let tipocc=$("#cc-tipo").val();
        let scadenza=$("#scadenza").val();
        let cvv=$("#cvv").val();

        if (!nome || !cognome || !Indirizzo || !cap || !citta || !paese || !tel || !controllomail(mail) || !passw ||!ripassw || !(ripassw===passw) ||!cc || !(cc.length===16) || !tipocc || !scadenza ||!cvv || !(cvv.length===3 )){
                if (!nome){
                $("#label-nome").addClass("error");
                $("#nome").addClass("Errore-input");
                $("#nome").removeClass("Inputok");
                $("#label-nome").removeClass("ok");

            }else {

                $("#nome").removeClass("Errore-input");
                $("#nome").addClass("Inputok");
                $("#label-nome").addClass("ok");
                $("#label-nome").removeClass("error");
            }

            if (!cognome){
                $("#label-cognome").addClass("error");
                $("#cognome").addClass("Errore-input");
                $("#cognome").removeClass("Inputok");
                $("#label-cognome").removeClass("ok");

            }else {

                $("#cognome").removeClass("Errore-input");
                $("#cognome").addClass("Inputok");
                $("#label-cognome").addClass("ok");
                $("#label-cognome").removeClass("error");
            }

            if (!Indirizzo){
                $("#label-indirizzo").addClass("error");
                $("#indirizzo").addClass("Errore-input");
                $("#indirizzo").removeClass("Inputok");
                $("#label-indirizzo").removeClass("ok");

            }else {

                $("#indirizzo").removeClass("Errore-input");
                $("#indirizzo").addClass("Inputok");
                $("#label-indirizzo").addClass("ok");
                $("#label-indirizzo").removeClass("error");
            }


            if (!cap){
                $("#label-CAP").addClass("error");
                $("#CAP").addClass("Errore-input");
                $("#CAP").removeClass("Inputok");
                $("#label-CAP").removeClass("ok");

            }else {

                $("#CAP").removeClass("Errore-input");
                $("#CAP").addClass("Inputok");
                $("#label-CAP").addClass("ok");
                $("#label-CAP").removeClass("error");
            }


            if (!citta){
                $("#label-citta").addClass("error");
                $("#citta").addClass("Errore-input");
                $("#citta").removeClass("Inputok");
                $("#label-citta").removeClass("ok");

            }else {

                $("#citta").removeClass("Errore-input");
                $("#citta").addClass("Inputok");
                $("#label-citta").addClass("ok");
                $("#label-citta").removeClass("error");
            }


            if (!paese){
                $("#label-paese").addClass("error");
                $("#paese").addClass("Errore-input");
                $("#paese").removeClass("Inputok");
                $("#label-paese").removeClass("ok");

            }else {

                $("#paese").removeClass("Errore-input");
                $("#paese").addClass("Inputok");
                $("#label-paese").addClass("ok");
                $("#label-paese").removeClass("error");
            }


            if (!tel){
                $("#label-tel").addClass("error");
                $("#tel").addClass("Errore-input");
                $("#tel").removeClass("Inputok");
                $("#label-tel").removeClass("ok");

            }else {

                $("#tel").removeClass("Errore-input");
                $("#tel").addClass("Inputok");
                $("#label-tel").addClass("ok");
                $("#label-tel").removeClass("error");
            }

            if (!controllomail(mail)){
                $("#label-email").addClass("error");
                $("#email").addClass("Errore-input");
                $("#email").removeClass("Inputok");
                $("#label-email").removeClass("ok");

            }else {

                $("#email").removeClass("Errore-input");
                $("#email").addClass("Inputok");
                $("#label-email").addClass("ok");
                $("#label-email").removeClass("error");
            }

            if (!passw){
                $("#label-pwd").addClass("error");
                $("#pwd").addClass("Errore-input");
                $("#pwd").removeClass("Inputok");
                $("#label-pwd").removeClass("ok");

            }else {

                $("#pwd").removeClass("Errore-input");
                $("#pwd").addClass("Inputok");
                $("#label-pwd").addClass("ok");
                $("#label-pwd").removeClass("error");
            }

            if (!passw || !(passw===ripassw)){
                $("#label-pwd-repeat").addClass("error");
                $("#pwd-repeat").addClass("Errore-input");
                $("#pwd-repeat").removeClass("Inputok");
                $("#label-pwd-repeat").removeClass("ok");

            }else {

                $("#pwd-repeat").removeClass("Errore-input");
                $("#pwd-repeat").addClass("Inputok");
                $("#label-pwd-repeat").addClass("ok");
                $("#label-pwd-repeat").removeClass("error");
            }

            if (!cc || !(cc.length===16) ){

                $("#label-cc").addClass("error");
                $("#cc").addClass("Errore-input");
                $("#cc").removeClass("Inputok");
                $("#label-cc").removeClass("ok");

            }else {

                $("#cc").removeClass("Errore-input");
                $("#cc").addClass("Inputok");
                $("#label-cc").addClass("ok");
                $("#label-cc").removeClass("error");
            }


            if (!tipocc){
                $("#label-cc-tipo").addClass("error");
                $("#cc-tipo").addClass("Errore-input");
                $("#cc-tipo").removeClass("Inputok");
                $("#label-cc-tipo").removeClass("ok");

            }else {

                $("#cc-tipo").removeClass("Errore-input");
                $("#cc-tipo").addClass("Inputok");
                $("#label-cc-tipo").addClass("ok");
                $("#label-cc-tipo").removeClass("error");
            }



            if (!scadenza){
                $("#par-scad").addClass("error");
                $("#scadenza").addClass("Errore-input");
                $("#scadenza").removeClass("Inputok");
                $("#par-scad").removeClass("ok");

            }else {
                $("#par-scad").removeClass("error");
                $("#scadenza").removeClass("Errore-input");
                $("#scadenza").addClass("Inputok");
                $("#par-scad").addClass("ok");
            }

            if (!cvv || !(cvv.length===3)){


                $("#label-cvv").addClass("error");
                $("#cvv").addClass("Errore-input");
                $("#cvv").removeClass("Inputok");
                $("#label-cvv").removeClass("ok");

            }else {

                $("#cvv").removeClass("Errore-input");
                $("#cvv").addClass("Inputok");
                $("#label-cvv").addClass("ok");
                $("#label-cvv").removeClass("error");
            }

        }else {
            $.post("index.php",{
                value: "registrazione",
                nome:nome,
                cognome:cognome,
                indirizzo:Indirizzo,
                cap:cap,
                citta:citta,
                paese:paese,
                tel:tel,
                mail:mail,
                passw:passw,
                cc:cc,
                tipocc:tipocc,
                scadenza:scadenza,
                cvv:cvv

            },function (data) {
                    console.log(data);
            })
        }



    })


    $("#submit-login").click(function (e) {
        e.preventDefault();
        let loginmail=$("#email-login").val();
        let loginpassw=$("#pwd-login").val();
        if (!controllomail(loginmail) || !loginpassw){
            if (!controllomail(loginmail)){
                $("#label-email-login").addClass("error");
                $("#email-login").addClass("Errore-input");
                $("#email-login").removeClass("Inputok");
                $("#label-email-login").removeClass("ok");

            }else {

                $("#email-login").removeClass("Errore-input");
                $("#email-login").addClass("Inputok");
                $("#label-email-login").addClass("ok");
                $("#label-email-login").removeClass("error");
            }


            if (!loginpassw){
                $("#label-pwd-login").addClass("error");
                $("#pwd-login").addClass("Errore-input");
                $("#pwd-login").removeClass("Inputok");
                $("#label-pwd-login").removeClass("ok");

            }else {

                $("#pwd-login").removeClass("Errore-input");
                $("#pwd-login").addClass("Inputok");
                $("#label-pwd-login").addClass("ok");
                $("#label-pwd-login").removeClass("error");
            }


        }else{
            $.post("index.php",{
                value:"login",
                loginmail: loginmail,
                loginpassw:loginpassw
            },function (data) {
                console.log(data);

            })
        }


    })

    function controllomail(mail){
        return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) ? true : false;
    }

})