-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Apr 14, 2020 alle 19:56
-- Versione del server: 10.4.11-MariaDB
-- Versione PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `libreria`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `dettagli_ordine`
--

CREATE TABLE `dettagli_ordine` (
  `id` int(11) NOT NULL,
  `id_libro` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL,
  `quantita` int(11) NOT NULL,
  `prezzo_scontato` float NOT NULL,
  `stato` varchar(50) NOT NULL,
  `evasione_prevista` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `libri`
--

CREATE TABLE `libri` (
  `id_libro` int(11) NOT NULL,
  `titolo` varchar(50) NOT NULL,
  `autore` varchar(50) NOT NULL,
  `nazione` varchar(50) NOT NULL,
  `lingua` varchar(50) NOT NULL,
  `pagine` int(11) NOT NULL,
  `anno` int(11) NOT NULL,
  `copertina` varchar(50) NOT NULL,
  `prezzo` float NOT NULL,
  `sconto` int(11) NOT NULL,
  `id_reparto` int(11) NOT NULL,
  `data_archiviazione` date DEFAULT curdate()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `libri`
--

INSERT INTO `libri` (`id_libro`, `titolo`, `autore`, `nazione`, `lingua`, `pagine`, `anno`, `copertina`, `prezzo`, `sconto`, `id_reparto`, `data_archiviazione`) VALUES
(1, 'Things Fall Apart', 'Chinua Achebe', 'Nigeria', 'English', 209, 1958, 'things-fall-apart.jpg', 25.9, 25, 3, '2020-04-14'),
(2, 'Fairy tales', 'Hans Christian Andersen', 'Denmark', 'Danish', 784, 1836, 'fairy-tales.jpg', 45.5, 0, 6, '2020-04-14'),
(3, 'The Divine Comedy', 'Dante Alighieri', 'Italy', 'Italian', 928, 1315, 'the-divine-comedy.jpg', 37.4, 20, 1, '2020-04-14'),
(4, 'The Epic Of Gilgamesh', 'Unknown', 'Sumer and Akkadian Empire', 'Akkadian', 160, -1700, 'the-epic-of-gilgamesh.jpg', 46.2, 25, 6, '2020-04-14'),
(5, 'The Book Of Job', 'Unknown', 'Achaemenid Empire', 'Hebrew', 176, -600, 'the-book-of-job.jpg', 16.6, 20, 7, '2020-04-14'),
(6, 'One Thousand and One Nights', 'Unknown', 'India/Iran/Iraq/Egypt/Tajikistan', 'Arabic', 288, 1200, 'one-thousand-and-one-nights.jpg', 15.5, 50, 6, '2020-04-14'),
(7, 'Njál\'s Saga', 'Unknown', 'Iceland', 'Old Norse', 384, 1350, 'njals-saga.jpg', 46.4, 20, 1, '2020-04-14'),
(8, 'Pride and Prejudice', 'Jane Austen', 'United Kingdom', 'English', 226, 1813, 'pride-and-prejudice.jpg', 46.6, 50, 7, '2020-04-14'),
(9, 'Le Père Goriot', 'Honoré de Balzac', 'France', 'French', 443, 1835, 'le-pere-goriot.jpg', 9.7, 20, 1, '2020-04-14'),
(10, 'Molloy, Malone Dies, The Unnamable, the trilogy', 'Samuel Beckett', 'Republic of Ireland', 'French, English', 256, 1952, 'molloy-malone-dies-the-unnamable.jpg', 39.9, 0, 7, '2020-04-14'),
(11, 'The Decameron', 'Giovanni Boccaccio', 'Italy', 'Italian', 1024, 1351, 'the-decameron.jpg', 5, 20, 7, '2020-04-14'),
(12, 'Ficciones', 'Jorge Luis Borges', 'Argentina', 'Spanish', 224, 1965, 'ficciones.jpg', 18.2, 20, 6, '2020-04-14'),
(13, 'Wuthering Heights', 'Emily Brontë', 'United Kingdom', 'English', 342, 1847, 'wuthering-heights.jpg', 39.9, 0, 3, '2020-04-14'),
(14, 'The Stranger', 'Albert Camus', 'Algeria, French Empire', 'French', 185, 1942, 'l-etranger.jpg', 29.4, 25, 7, '2020-04-14'),
(15, 'Poems', 'Paul Celan', 'Romania, France', 'German', 320, 1952, 'poems-paul-celan.jpg', 6.5, 50, 7, '2020-04-14'),
(16, 'Journey to the End of the Night', 'Louis-Ferdinand Céline', 'France', 'French', 505, 1932, 'voyage-au-bout-de-la-nuit.jpg', 27.9, 25, 2, '2020-04-14'),
(17, 'Don Quijote De La Mancha', 'Miguel de Cervantes', 'Spain', 'Spanish', 1056, 1610, 'don-quijote-de-la-mancha.jpg', 42.3, 50, 9, '2020-04-14'),
(18, 'The Canterbury Tales', 'Geoffrey Chaucer', 'England', 'English', 544, 1450, 'the-canterbury-tales.jpg', 5.4, 20, 3, '2020-04-14'),
(19, 'Stories', 'Anton Chekhov', 'Russia', 'Russian', 194, 1886, 'stories-of-anton-chekhov.jpg', 21.7, 0, 9, '2020-04-14'),
(20, 'Nostromo', 'Joseph Conrad', 'United Kingdom', 'English', 320, 1904, 'nostromo.jpg', 48.7, 25, 4, '2020-04-14'),
(21, 'Great Expectations', 'Charles Dickens', 'United Kingdom', 'English', 194, 1861, 'great-expectations.jpg', 22.7, 25, 4, '2020-04-14'),
(22, 'Jacques the Fatalist', 'Denis Diderot', 'France', 'French', 596, 1796, 'jacques-the-fatalist.jpg', 28.8, 20, 7, '2020-04-14'),
(23, 'Berlin Alexanderplatz', 'Alfred Döblin', 'Germany', 'German', 600, 1929, 'berlin-alexanderplatz.jpg', 38.3, 25, 1, '2020-04-14'),
(24, 'Crime and Punishment', 'Fyodor Dostoevsky', 'Russia', 'Russian', 551, 1866, 'crime-and-punishment.jpg', 23.1, 20, 4, '2020-04-14'),
(25, 'The Idiot', 'Fyodor Dostoevsky', 'Russia', 'Russian', 656, 1869, 'the-idiot.jpg', 48.2, 0, 5, '2020-04-14'),
(26, 'The Possessed', 'Fyodor Dostoevsky', 'Russia', 'Russian', 768, 1872, 'the-possessed.jpg', 21.3, 20, 2, '2020-04-14'),
(27, 'The Brothers Karamazov', 'Fyodor Dostoevsky', 'Russia', 'Russian', 824, 1880, 'the-brothers-karamazov.jpg', 15.2, 25, 8, '2020-04-14'),
(28, 'Middlemarch', 'George Eliot', 'United Kingdom', 'English', 800, 1871, 'middlemarch.jpg', 20.5, 25, 2, '2020-04-14'),
(29, 'Invisible Man', 'Ralph Ellison', 'United States', 'English', 581, 1952, 'invisible-man.jpg', 21.7, 25, 9, '2020-04-14'),
(30, 'Medea', 'Euripides', 'Greece', 'Greek', 104, -431, 'medea.jpg', 47.6, 20, 9, '2020-04-14'),
(31, 'Absalom, Absalom!', 'William Faulkner', 'United States', 'English', 313, 1936, 'absalom-absalom.jpg', 47.4, 50, 1, '2020-04-14'),
(32, 'The Sound and the Fury', 'William Faulkner', 'United States', 'English', 326, 1929, 'the-sound-and-the-fury.jpg', 40.8, 20, 2, '2020-04-14'),
(33, 'Madame Bovary', 'Gustave Flaubert', 'France', 'French', 528, 1857, 'madame-bovary.jpg', 21.2, 25, 4, '2020-04-14'),
(34, 'Sentimental Education', 'Gustave Flaubert', 'France', 'French', 606, 1869, 'l-education-sentimentale.jpg', 32.5, 0, 2, '2020-04-14'),
(35, 'Gypsy Ballads', 'Federico García Lorca', 'Spain', 'Spanish', 218, 1928, 'gypsy-ballads.jpg', 43.9, 25, 9, '2020-04-14'),
(36, 'One Hundred Years of Solitude', 'Gabriel García Márquez', 'Colombia', 'Spanish', 417, 1967, 'one-hundred-years-of-solitude.jpg', 41.8, 20, 9, '2020-04-14'),
(37, 'Love in the Time of Cholera', 'Gabriel García Márquez', 'Colombia', 'Spanish', 368, 1985, 'love-in-the-time-of-cholera.jpg', 8.3, 25, 8, '2020-04-14'),
(38, 'Faust', 'Johann Wolfgang von Goethe', 'Saxe-Weimar', 'German', 158, 1832, 'faust.jpg', 35.3, 20, 1, '2020-04-14'),
(39, 'Dead Souls', 'Nikolai Gogol', 'Russia', 'Russian', 432, 1842, 'dead-souls.jpg', 37.1, 0, 2, '2020-04-14'),
(40, 'The Tin Drum', 'Günter Grass', 'Germany', 'German', 600, 1959, 'the-tin-drum.jpg', 38.5, 50, 8, '2020-04-14'),
(41, 'The Devil to Pay in the Backlands', 'João Guimarães Rosa', 'Brazil', 'Portuguese', 494, 1956, 'the-devil-to-pay-in-the-backlands.jpg', 23.5, 20, 2, '2020-04-14'),
(42, 'Hunger', 'Knut Hamsun', 'Norway', 'Norwegian', 176, 1890, 'hunger.jpg', 47.4, 25, 9, '2020-04-14'),
(43, 'The Old Man and the Sea', 'Ernest Hemingway', 'United States', 'English', 128, 1952, 'the-old-man-and-the-sea.jpg', 6.9, 0, 6, '2020-04-14'),
(44, 'Iliad', 'Homer', 'Greece', 'Greek', 608, -735, 'the-iliad-of-homer.jpg', 21.3, 0, 8, '2020-04-14'),
(45, 'Odyssey', 'Homer', 'Greece', 'Greek', 374, -800, 'the-odyssey-of-homer.jpg', 47.4, 50, 6, '2020-04-14'),
(46, 'A Doll\'s House', 'Henrik Ibsen', 'Norway', 'Norwegian', 68, 1879, 'a-Dolls-house.jpg', 20.1, 50, 1, '2020-04-14'),
(47, 'Ulysses', 'James Joyce', 'Irish Free State', 'English', 228, 1922, 'ulysses.jpg', 40.8, 20, 7, '2020-04-14'),
(48, 'Stories', 'Franz Kafka', 'Czechoslovakia', 'German', 488, 1924, 'stories-of-franz-kafka.jpg', 31.2, 50, 8, '2020-04-14'),
(49, 'The Trial', 'Franz Kafka', 'Czechoslovakia', 'German', 160, 1925, 'the-trial.jpg', 44.3, 0, 4, '2020-04-14'),
(50, 'The Castle', 'Franz Kafka', 'Czechoslovakia', 'German', 352, 1926, 'the-castle.jpg', 20.3, 20, 1, '2020-04-14'),
(51, 'The recognition of Shakuntala', 'Kālidāsa', 'India', 'Sanskrit', 147, 150, 'the-recognition-of-shakuntala.jpg', 32.2, 0, 9, '2020-04-14'),
(52, 'The Sound of the Mountain', 'Yasunari Kawabata', 'Japan', 'Japanese', 288, 1954, 'the-sound-of-the-mountain.jpg', 20.4, 50, 4, '2020-04-14'),
(53, 'Zorba the Greek', 'Nikos Kazantzakis', 'Greece', 'Greek', 368, 1946, 'zorba-the-greek.jpg', 32.2, 25, 5, '2020-04-14'),
(54, 'Sons and Lovers', 'D. H. Lawrence', 'United Kingdom', 'English', 432, 1913, 'sons-and-lovers.jpg', 34.5, 20, 5, '2020-04-14'),
(55, 'Independent People', 'Halldór Laxness', 'Iceland', 'Icelandic', 470, 1934, 'independent-people.jpg', 35.2, 20, 4, '2020-04-14'),
(56, 'Poems', 'Giacomo Leopardi', 'Italy', 'Italian', 184, 1818, 'poems-giacomo-leopardi.jpg', 44.4, 25, 1, '2020-04-14'),
(57, 'The Golden Notebook', 'Doris Lessing', 'United Kingdom', 'English', 688, 1962, 'the-golden-notebook.jpg', 29.1, 0, 1, '2020-04-14'),
(58, 'Pippi Longstocking', 'Astrid Lindgren', 'Sweden', 'Swedish', 160, 1945, 'pippi-longstocking.jpg', 29.6, 50, 2, '2020-04-14'),
(59, 'Diary of a Madman', 'Lu Xun', 'China', 'Chinese', 389, 1918, 'diary-of-a-madman.jpg', 47.9, 20, 3, '2020-04-14'),
(60, 'Children of Gebelawi', 'Naguib Mahfouz', 'Egypt', 'Arabic', 355, 1959, 'children-of-gebelawi.jpg', 35.9, 0, 2, '2020-04-14'),
(61, 'Buddenbrooks', 'Thomas Mann', 'Germany', 'German', 736, 1901, 'buddenbrooks.jpg', 22.2, 0, 7, '2020-04-14'),
(62, 'The Magic Mountain', 'Thomas Mann', 'Germany', 'German', 720, 1924, 'the-magic-mountain.jpg', 7.1, 50, 8, '2020-04-14'),
(63, 'Moby Dick', 'Herman Melville', 'United States', 'English', 378, 1851, 'moby-dick.jpg', 24.4, 20, 2, '2020-04-14'),
(64, 'Essays', 'Michel de Montaigne', 'France', 'French', 404, 1595, 'essais.jpg', 32.1, 50, 5, '2020-04-14'),
(65, 'History', 'Elsa Morante', 'Italy', 'Italian', 600, 1974, 'history.jpg', 47.6, 0, 7, '2020-04-14'),
(66, 'Beloved', 'Toni Morrison', 'United States', 'English', 321, 1987, 'beloved.jpg', 22.5, 25, 2, '2020-04-14'),
(67, 'The Tale of Genji', 'Murasaki Shikibu', 'Japan', 'Japanese', 1360, 1006, 'the-tale-of-genji.jpg', 9.8, 0, 2, '2020-04-14'),
(68, 'The Man Without Qualities', 'Robert Musil', 'Austria', 'German', 365, 1931, 'the-man-without-qualities.jpg', 44, 20, 6, '2020-04-14'),
(69, 'Lolita', 'Vladimir Nabokov', 'Russia/United States', 'English', 317, 1955, 'lolita.jpg', 16.5, 20, 3, '2020-04-14'),
(70, 'Nineteen Eighty-Four', 'George Orwell', 'United Kingdom', 'English', 272, 1949, 'nineteen-eighty-four.jpg', 38.1, 20, 1, '2020-04-14'),
(71, 'Metamorphoses', 'Ovid', 'Roman Empire', 'Classical Latin', 576, 100, 'the-metamorphoses-of-ovid.jpg', 49.7, 25, 4, '2020-04-14'),
(72, 'The Book of Disquiet', 'Fernando Pessoa', 'Portugal', 'Portuguese', 272, 1928, 'the-book-of-disquiet.jpg', 16, 50, 2, '2020-04-14'),
(73, 'Tales', 'Edgar Allan Poe', 'United States', 'English', 842, 1950, 'tales-and-poems-of-edgar-allan-poe.jpg', 26.8, 25, 4, '2020-04-14'),
(74, 'In Search of Lost Time', 'Marcel Proust', 'France', 'French', 2408, 1920, 'a-la-recherche-du-temps-perdu.jpg', 43.2, 25, 4, '2020-04-14'),
(75, 'Gargantua and Pantagruel', 'François Rabelais', 'France', 'French', 623, 1533, 'gargantua-and-pantagruel.jpg', 31.1, 50, 3, '2020-04-14'),
(76, 'Pedro Páramo', 'Juan Rulfo', 'Mexico', 'Spanish', 124, 1955, 'pedro-paramo.jpg', 5, 25, 1, '2020-04-14'),
(77, 'The Masnavi', 'Rumi', 'Sultanate of Rum', 'Persian', 438, 1236, 'the-masnavi.jpg', 46.1, 0, 9, '2020-04-14'),
(78, 'Midnight\'s Children', 'Salman Rushdie', 'United Kingdom, India', 'English', 536, 1981, 'midnights-children.jpg', 45, 50, 6, '2020-04-14'),
(79, 'Bostan', 'Saadi', 'Persia, Persian Empire', 'Persian', 298, 1257, 'bostan.jpg', 27.5, 0, 8, '2020-04-14'),
(80, 'Season of Migration to the North', 'Tayeb Salih', 'Sudan', 'Arabic', 139, 1966, 'season-of-migration-to-the-north.jpg', 23.6, 25, 7, '2020-04-14'),
(81, 'Blindness', 'José Saramago', 'Portugal', 'Portuguese', 352, 1995, 'blindness.jpg', 28, 0, 4, '2020-04-14'),
(82, 'Hamlet', 'William Shakespeare', 'England', 'English', 432, 1603, 'hamlet.jpg', 29, 0, 1, '2020-04-14'),
(83, 'King Lear', 'William Shakespeare', 'England', 'English', 384, 1608, 'king-lear.jpg', 48.4, 0, 2, '2020-04-14'),
(84, 'Othello', 'William Shakespeare', 'England', 'English', 314, 1609, 'othello.jpg', 27.9, 50, 3, '2020-04-14'),
(85, 'Oedipus the King', 'Sophocles', 'Greece', 'Greek', 88, -430, 'oedipus-the-king.jpg', 43.1, 20, 7, '2020-04-14'),
(86, 'The Red and the Black', 'Stendhal', 'France', 'French', 576, 1830, 'le-rouge-et-le-noir.jpg', 13.8, 0, 9, '2020-04-14'),
(87, 'The Life And Opinions of Tristram Shandy', 'Laurence Sterne', 'England', 'English', 640, 1760, 'the-life-and-opinions-of-tristram-shandy.jpg', 41, 0, 6, '2020-04-14'),
(88, 'Confessions of Zeno', 'Italo Svevo', 'Italy', 'Italian', 412, 1923, 'confessions-of-zeno.jpg', 36.6, 0, 7, '2020-04-14'),
(89, 'Gulliver\'s Travels', 'Jonathan Swift', 'Ireland', 'English', 178, 1726, 'gullivers-travels.jpg', 44.3, 50, 5, '2020-04-14'),
(90, 'War and Peace', 'Leo Tolstoy', 'Russia', 'Russian', 1296, 1867, 'war-and-peace.jpg', 28.9, 25, 4, '2020-04-14'),
(91, 'Anna Karenina', 'Leo Tolstoy', 'Russia', 'Russian', 864, 1877, 'anna-karenina.jpg', 9.7, 50, 6, '2020-04-14'),
(92, 'The Death of Ivan Ilyich', 'Leo Tolstoy', 'Russia', 'Russian', 92, 1886, 'the-death-of-ivan-ilyich.jpg', 46.7, 0, 7, '2020-04-14'),
(93, 'The Adventures of Huckleberry Finn', 'Mark Twain', 'United States', 'English', 224, 1884, 'the-adventures-of-huckleberry-finn.jpg', 19, 25, 3, '2020-04-14'),
(94, 'Ramayana', 'Valmiki', 'India', 'Sanskrit', 152, -450, 'ramayana.jpg', 5.6, 0, 4, '2020-04-14'),
(95, 'The Aeneid', 'Virgil', 'Roman Empire', 'Classical Latin', 442, -23, 'the-aeneid.jpg', 49.4, 20, 8, '2020-04-14'),
(96, 'Mahabharata', 'Vyasa', 'India', 'Sanskrit', 276, -700, 'the-mahab-harata.jpg', 23.2, 25, 7, '2020-04-14'),
(97, 'Leaves of Grass', 'Walt Whitman', 'United States', 'English', 152, 1855, 'leaves-of-grass.jpg', 9.5, 25, 3, '2020-04-14'),
(98, 'Mrs Dalloway', 'Virginia Woolf', 'United Kingdom', 'English', 216, 1925, 'mrs-dalloway.jpg', 26.9, 50, 3, '2020-04-14'),
(99, 'To the Lighthouse', 'Virginia Woolf', 'United Kingdom', 'English', 209, 1927, 'to-the-lighthouse.jpg', 23.1, 50, 8, '2020-04-14'),
(100, 'Memoirs of Hadrian', 'Marguerite Yourcenar', 'France/Belgium', 'French', 408, 1951, 'memoirs-of-hadrian.jpg', 37.8, 25, 8, '2020-04-14');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE `ordini` (
  `id_ordine` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `id_utente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `reparti`
--

CREATE TABLE `reparti` (
  `id_reparto` int(11) NOT NULL,
  `nome_reparto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `reparti`
--

INSERT INTO `reparti` (`id_reparto`, `nome_reparto`) VALUES
(1, 'Achitettura e urbanistica'),
(2, 'Arte'),
(3, 'Classici Greci e Latini'),
(4, 'Cucina e casa'),
(5, 'Diritto'),
(6, 'Economia e management'),
(7, 'Fantascienza e fantasy'),
(8, 'Filosofia'),
(9, 'Fumetti');

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `cognome` varchar(50) NOT NULL,
  `indirizzo` varchar(50) NOT NULL,
  `cap` varchar(50) NOT NULL,
  `citta` varchar(50) NOT NULL,
  `nazione` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `passw` varchar(50) NOT NULL,
  `cartacredito` varchar(50) NOT NULL,
  `tipocarta` varchar(50) NOT NULL,
  `scadenzacarta` date NOT NULL,
  `cvv` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `dettagli_ordine`
--
ALTER TABLE `dettagli_ordine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_libro` (`id_libro`),
  ADD KEY `id_ordine` (`id_ordine`);

--
-- Indici per le tabelle `libri`
--
ALTER TABLE `libri`
  ADD PRIMARY KEY (`id_libro`),
  ADD KEY `id_reparto` (`id_reparto`);

--
-- Indici per le tabelle `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`id_ordine`),
  ADD UNIQUE KEY `numero` (`numero`),
  ADD KEY `id_utente` (`id_utente`);

--
-- Indici per le tabelle `reparti`
--
ALTER TABLE `reparti`
  ADD PRIMARY KEY (`id_reparto`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `dettagli_ordine`
--
ALTER TABLE `dettagli_ordine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `libri`
--
ALTER TABLE `libri`
  MODIFY `id_libro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT per la tabella `ordini`
--
ALTER TABLE `ordini`
  MODIFY `id_ordine` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `reparti`
--
ALTER TABLE `reparti`
  MODIFY `id_reparto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT per la tabella `utenti`
--
ALTER TABLE `utenti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `dettagli_ordine`
--
ALTER TABLE `dettagli_ordine`
  ADD CONSTRAINT `dettagli_ordine_ibfk_1` FOREIGN KEY (`id_libro`) REFERENCES `libri` (`id_libro`),
  ADD CONSTRAINT `dettagli_ordine_ibfk_2` FOREIGN KEY (`id_ordine`) REFERENCES `ordini` (`id_ordine`);

--
-- Limiti per la tabella `libri`
--
ALTER TABLE `libri`
  ADD CONSTRAINT `libri_ibfk_1` FOREIGN KEY (`id_reparto`) REFERENCES `reparti` (`id_reparto`);

--
-- Limiti per la tabella `ordini`
--
ALTER TABLE `ordini`
  ADD CONSTRAINT `ordini_ibfk_1` FOREIGN KEY (`id_utente`) REFERENCES `utenti` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
